using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using KeepStock.UserInfo.Logic.ViewModels;
using Splat;

namespace UserInfoTool.Views
{
    public class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void login_Clicked(object sender, RoutedEventArgs args)
        {
            var login = new LoginView
                {
                    DataContext = Locator.Current.GetService<ILoginViewModel>()
                };
                login.ShowDialog(this);
        }
    }
}