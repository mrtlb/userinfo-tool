using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using KeepStock.UserInfo.Logic;
using KeepStock.UserInfo.Logic.ViewModels;
using Splat;
using Splat.Microsoft.Extensions.DependencyInjection;
using UserInfoTool.Views;

namespace UserInfoTool
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            var container = Bootstrapper.Init();
            container.UseMicrosoftDependencyResolver();
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainView
                {
                    DataContext = Locator.Current.GetService<IMainViewModel>()
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}