# UserInfoTool

Cross platform Grainger directory services tool.

## Overview

This tool is written in [AnvaloniaUI][AvaloniaUI] (using [ReactiveUI][ReactiveUI] and MVVM) which provides a cross platform UI and should be useable on Mac, Linux, and Windows desktops.  It should be possible to package the system as a single file executable (and even include .NET 5.0 dependencies).  So it should be possible to deploy even to systems that don't have .NET 5.0 installed.

The tool allows exploration of AD and LDAP system based on a user login.  It will also explore the AD trust forest.

It can be used to find AD user groups and LDAP information detail for one or more users.

**Note:**

* This is a .NET 5.0 application and you will need the [.NET 5.0 SDK][NETSdk] installed in order to build.
* This application has been built and run from Visual Studio Code.  The .vscode configuration is set up for running and launching.
* Unit tests are xUnit based.

## TODO Items

* ~~Add Microsoft DependencyInjections and remove Splat IoC~~
* Add Serilog
* [Localization][Localization] XAML helper
  * Add localization string resources

[AvaloniaUI]:   https://www.avaloniaui.net/
[ReactiveUI]:   https://www.reactiveui.net/
[NETSdk]:       https://dotnet.microsoft.com/download
[Localization]: https://www.sakya.it/wordpress/avalonia-ui-framework-localization/
