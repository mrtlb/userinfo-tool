﻿using System;
using System.Collections.Generic;
using System.Text;
using KeepStock.DirectoryServices;
using Microsoft.Extensions.Logging;

namespace KeepStock.UserInfo.Logic.ViewModels
{
    public interface IMainViewModel
    {
        string Greeting { get; }
    }

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private readonly IDirectoryLogic logic;
        private readonly ILogger<MainViewModel> logger;
        public MainViewModel(IDirectoryLogic logic, ILogger<MainViewModel> logger)
        {
            this.logger = logger;
            this.logic = logic;
            logger.LogInformation("Testing {@MainViewModel}", this);
        }
        public string Greeting => "Hello World!";
    }
}
