using System.Reactive;
using KeepStock.DirectoryServices;
using ReactiveUI;

namespace KeepStock.UserInfo.Logic.ViewModels
{
    public interface ILoginViewModel
    {
        string UserName { get; set; }
        string Password { get; set; }
        ReactiveCommand<Unit, bool> Ok { get; }
        ReactiveCommand<Unit, Unit> Cancel { get; }
    }

    public class LoginViewModel : ViewModelBase, ILoginViewModel
    {
        private string _userName;
        private string _password;
        private readonly IDirectoryLogic directoryLogic;

        public LoginViewModel(IDirectoryLogic directoryLogic)
        {
            this.directoryLogic = directoryLogic;

            var okEnabled = this.WhenAnyValue(
                x => x.UserName,
                x => x.Password,
                (u, p) => !string.IsNullOrWhiteSpace(u) && !string.IsNullOrWhiteSpace(p)
            );

            Ok = ReactiveCommand.Create<Unit, bool>(
                _ => directoryLogic.Authenticate(UserName, Password),
                okEnabled
            );
        }

        public string UserName
        {
            get => _userName;
            set => this.RaiseAndSetIfChanged(ref _userName, value);
        }
        public string Password
        {
            get => _password;
            set => this.RaiseAndSetIfChanged(ref _password, value);
        }

        public ReactiveCommand<Unit, bool> Ok { get; }
        public ReactiveCommand<Unit, Unit> Cancel { get; }
    }
}