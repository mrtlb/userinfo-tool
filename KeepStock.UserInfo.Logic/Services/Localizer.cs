﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using Avalonia.Platform;
using Microsoft.Extensions.DependencyInjection;
using Splat;
using KeepStock.UserInfo.Logic.Code;

namespace KeepStock.UserInfo.Logic.Services
{
    public interface ILocalizer
    {
        protected const string _indexerName = "Item";
        protected const string _indexerArrayName = "Item[]";
        public const string fallbackLanguage = "en-US";
        string Language { get; }
        bool HasFallback { get; }
        static ILocalizer Instance { get; }
        string this[string index] { get; }
        bool LoadLanguage(string language, bool useFallback = true, string fallbackLanguage = "en-US");
        event PropertyChangedEventHandler PropertyChanged;
        void Invalidate();
    }

    public class Localizer : INotifyPropertyChanged, ILocalizer
    {
        private readonly IAssetLoader _loader;
        private ImmutableDictionary<string, string> _strings = null;

        private Localizer(IAssetLoader loader)
        {
            _loader = loader;
            LoadLanguage(ILocalizer.fallbackLanguage);
        }

        public static ILocalizer Instance { get; private set; }

        public static ILocalizer BuildLocalizer(IServiceProvider provider)
        {
            Localizer.Instance = new Localizer(provider.GetService<IAssetLoader>());
            return Localizer.Instance;
        }

        public bool LoadLanguage(string language, bool useFallback = true, string fallbackLanguage = ILocalizer.fallbackLanguage)
        {
            Language = language;
            HasFallback = useFallback;

            var assemblyName = this.GetType().Assembly.GetName().Name;
            var locale = language;

            FormattableString localResoursource = $"avares://{assemblyName}//Assets//i18n//Strings_{locale}.json";
            
            //TODO  Load String resources and fallbacks here.
            // https://www.sakya.it/wordpress/avalonia-ui-framework-localization/
            return false;
        }

        public string Language { get; private set; }
        public bool HasFallback { get; private set; }

        public string this[string index]
        {
            get
            {
                string res=null;
                if (_strings?.TryGetValue(index, out res) ?? false)
                {
                    return res?.Replace("\\n", "\n");
                }

                return $"{Language}:{index}";
            }
        }

        public void Invalidate()
        {
            PropertyChanged.Raise(this, ILocalizer._indexerName);
            PropertyChanged.Raise(this, ILocalizer._indexerArrayName);
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}