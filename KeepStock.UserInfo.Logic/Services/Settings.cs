﻿using System;
using Serilog.Events;
using KeepStock.DirectoryServices;

namespace KeepStock.UserInfo.Logic.Services
{
    public interface ISettings
    {
        Settings.LoggingSettings Logging { get; set; }
        ILDAPSettings LDAP {get; set;}
        
        //Helpers
        string AppDataFolder { get; }
        string DocumentsFolder { get; }
    }

    public class Settings : ISettings
    {
        public LoggingSettings Logging { get; set; } = new LoggingSettings();
        public ILDAPSettings LDAP { get; set; } = new LDAPSettings();

        public string AppDataFolder =>
            $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}/UserInfo";
        public string DocumentsFolder =>
            $"{Environment.GetFolderPath(Environment.SpecialFolder.Personal)}";

        public class LoggingSettings
        {
            public LogEventLevel LogLevel { get; set; } = LogEventLevel.Error;
            public string[] Loggers { get; set; } = Array.Empty<string>();
        }

        public class LDAPSettings : ILDAPSettings
        {
            public string Server { get; set; } = "pridv01.lf.grainger.com";
            public string BaseDN { get; set; } = "ou=users,o=Grainger";
            public int Port { get; set; } = 636;

            public string Credentials { get; set; } = "User=cn=zTBAccess,ou=srv,ou=chi,o=grainger;Password=wwg@tb4!";

            public bool IgnoreSSLErrors { get; set; } = false;



        }
    }
}