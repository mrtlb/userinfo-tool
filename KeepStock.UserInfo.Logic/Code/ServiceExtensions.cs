﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace KeepStock.UserInfo.Logic.Code
{
    public static class ServiceExtensions
    {
        public static void Raise(this PropertyChangedEventHandler  handler, object source, [CallerMemberName] string propertyName=null)
        {
            if (handler != null) handler(source, new PropertyChangedEventArgs(propertyName));
        }
        
        public static void Raise<T>(this PropertyChangedEventHandler  handler, object source, Expression<Func<T>> property)
        {
            string propertyName = ((MemberExpression)property.Body).Member.Name;
            handler.Raise(source, propertyName);
        }
    }
}