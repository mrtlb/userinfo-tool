﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using KeepStock.DirectoryServices;
using KeepStock.DirectoryServices.Factories;
using KeepStock.UserInfo.Logic.Services;
using KeepStock.UserInfo.Logic.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace KeepStock.UserInfo.Logic
{
    public class Bootstrapper
    {
        private const string AspnetcoreEnvironment = "ASPNETCORE_ENVIRONMENT";

        private const string _debugOUtputTemplate =
            "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext,-60} {Message:lj}{NewLine}{Exception}";

        private const string _consoleOutputTemplate =
            "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext,-60} {Message:lj}{NewLine}{Exception}";

        private const string _fileOutputTemplate =
            "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {SourceContext,-60} {Message:lj}{NewLine}{Exception}";

        public static IServiceCollection Init()
        {
            var container = new ServiceCollection();
            return RegisterServices(container);
        }

        private static IServiceCollection RegisterServices(ServiceCollection container)
        {
            return container

                //Fundamental elements
                .AddSingleton<IFileSystem, FileSystem>(provider => new FileSystem())
                .AddSingleton(provider => Localizer.BuildLocalizer(provider))
                .AddSingleton(provider => BuildConfiguration(provider))
                .AddSingleton(provider => BuildSettings(provider))
                .AddSingleton(provider => BuildLoggerFactory(provider))
                .AddSingleton(provider => provider.GetService<ISettings>().LDAP)
                .AddTransient(typeof(ILogger<>), typeof(Logger<>))

                //Application Services
                .AddSingleton<ILDAPFactory>(provider => new LDAPFactory())
                .AddSingleton<IADFactory>(provider => new ADFactory())
                .AddTransient<IDirectoryLogic, DirectoryLogic>()

                //ViewModels
                .AddTransient<IMainViewModel, MainViewModel>()
                .AddTransient<ILoginViewModel, LoginViewModel>();
        }

        private static ISettings BuildSettings(IServiceProvider provider)
        {
            var fs = provider.GetService<IFileSystem>();
            var settings = new Settings();
            provider.GetService<IConfiguration>().Bind(settings);
            //TODO   If there is no application settings file, make one.
            //if (!fs.File.Exists())
            return settings;
        }

        private static Dictionary<string, string> ConfigurationDefaults = new Dictionary<string, string>
        {
            //TODO  put built-in configuration here
        };

        private static IConfiguration BuildConfiguration(IServiceProvider serviceProvider)
        {
#if DEBUG
            Environment.SetEnvironmentVariable(AspnetcoreEnvironment, "debug");
#else
            Environment.SetEnvironmentVariable(AspnetcoreEnvironment,"release");
#endif
            var env = Environment.GetEnvironmentVariable(AspnetcoreEnvironment);
            var builder = new ConfigurationBuilder()
                .AddInMemoryCollection(ConfigurationDefaults)
                .AddJsonFile("userInfoSettings.json", true, true)
                .AddJsonFile($"userInfoSettings.{env}.json", true, true)
                .AddEnvironmentVariables()
                .AddCommandLine(Environment.GetCommandLineArgs());
            return builder.Build();
        }

        private static ILoggerFactory BuildLoggerFactory(IServiceProvider provider)
        {
            var config = provider.GetService<IConfiguration>();
            var settings = provider.GetService<ISettings>();
            var locale = provider.GetService<ILocalizer>();

            var loggerConfig = new LoggerConfiguration();

            if (settings != null)
            {
                loggerConfig.MinimumLevel.Is(settings.Logging.LogLevel);

                foreach (var loggerName in settings.Logging.Loggers)
                {
                    switch (loggerName.ToLower())
                    {
                        case "debug":
                            loggerConfig
                                .WriteTo.Debug(settings.Logging.LogLevel, _debugOUtputTemplate);
                            break;
                        case "console":
                            loggerConfig
                                .WriteTo.Console(settings.Logging.LogLevel, _consoleOutputTemplate);
                            break;
                        case "file":
                            loggerConfig
                                .WriteTo.File($"{settings.AppDataFolder}/userInfo.log",
                                    restrictedToMinimumLevel: settings.Logging.LogLevel,
                                    rollingInterval: RollingInterval.Day,
                                    outputTemplate: _fileOutputTemplate);
                            break;
                    }
                }
            }


            var logger = loggerConfig.CreateLogger();

            logger.ForContext<Bootstrapper>().Information(
                "Serilog configured -- Settings {@settings}", settings);

            return new LoggerFactory().AddSerilog(logger, true);
        }
    }

}
