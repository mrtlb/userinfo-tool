using System;
using KeepStock.DirectoryServices.Factories;
using Microsoft.Extensions.Logging;

namespace KeepStock.DirectoryServices
{

    public interface ILDAPSettings
    {
        string Server { get; set; }
        string BaseDN { get; set; }
        int Port { get; set; }
        string Credentials { get; set; }
        bool IgnoreSSLErrors { get; set; }
    }

    public interface IDirectoryLogic
    {
        bool Authenticate(string userName, string password);
    }

    /// <summary>
    /// Provides high-level functionality without having to descend into the inner working of LDAP or AD
    /// </summary>
    public class DirectoryLogic:IDirectoryLogic
    {
        private readonly ILDAPFactory ldapFactory;
        private readonly IADFactory adFactory;
        private readonly ILDAPSettings settings;
        private readonly ILogger<DirectoryLogic> logger;
        
        public DirectoryLogic(ILDAPFactory ldapFactory, IADFactory adFactory, ILDAPSettings settings, ILogger<DirectoryLogic> logger)
        {
            this.logger = logger;
            this.settings = settings;
            this.adFactory = adFactory;
            this.ldapFactory = ldapFactory;
            
        }

        public bool Authenticate(string userName, string password)
        {
            return true;
        }
    }


}